<?php
    include_once("functions.php");
    session_start();
?>
<head>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<style>
    body {
        display: grid;
        grid-template-columns: repeat(5, 1fr);
        
    }

    .wrapper {
        grid-column: 1 / 5;
        justify-self: center;
        justify-items: center;
    }

    select {
        margin-right: 20px;
    }
</style>

    <div class='wrapper'>

    <?php
//course submission form: get students from 
if(!isset($_POST['course'])) {
    foreach($courseResource as $course) {
        $cn = $course['name'];
        $cid = $course['id'];
        
        $sd = $course['start_at'];
        $sd = strtotime(substr($sd, 0, 10));
        
        $ed = $course['end_at'];
        $ed = strtotime(substr($ed, 0, 10));
        
        $td = strtotime(date("Y-m-d"));
        
        if($td > $st && $td < $ed) {

            $coursename[] = $cn;
            $courseid[] = $cid;
        }
    }
}
        
        
if(count($coursename) < 2 && !isset($_POST['course'])) {
    
    $assignment_api_url = "https://cehe.instructure.com/api/v1/courses/".$courseid[0]."/assignments?include[]=submission";
    $assignmentResource = getAssignment($access_token);
    $progress = 0;
    foreach($assignmentResource as $assignment) {
    
        $assnid = $assignment['id'];
        $assngrade = $assignment['submission']['grade'];
        $courseid2 = $assignment['course_id'];
        $assntotal++;
            if($assngrade != "" && $assngrade != "-" && $assngrade !=0 && $assngrade != 'null') {
                $progress++;
            }
    }
    $dec = $progress/$assntotal;
    $perc = round((float)$dec * 100);

    echo"
    <div class='w3-light-grey' style='border-radius: 25px;'>
            <div class= 'w3-container w3-center' style='width: ".$perc."%; border-radius: 25px; background-color: #013245; color: white;'>".$perc."%
            </div>
        </div>
        <div class='w3-center'>This progress bar shows the completed percentage of your main course tasks (e.g. discussions, assignments, assessments). 
        It is not your grade. It does not include your daily checkpoints.</div>
        <br />
        

    ";
}
else {

    if(!isset($_POST['course'])) {

            echo "
            <form method='post'>
                <select name='course'>
                <option></option>";
                for($i=0; $i < count($coursename); $i++) {

                    echo '<option value="'.$courseid[$i] . ',' . $coursename[$i].'">'.$coursename[$i].'</option>';
                }
                echo "</select>

                <input type='hidden' value='".$access_token."' name='access_token'>
                <input type='submit' value='Submit'>
            </form>";
    }
}

    function after ($something, $inthat) {
        if (!is_bool(strpos($inthat, $something)))
        return substr($inthat, strpos($inthat,$something)+strlen($something));
    };
    function before ($something, $inthat) {
        return substr($inthat, 0, strpos($inthat, $something));
    };

    if(isset($_POST['course'])) {
    
        $course = $_POST['course'];
        
        $coursename = after(",", $course);
        $courseid = before(",", $course);

        $access_token = $_POST['access_token'];
      
    $assignment_api_url = "https://cehe.instructure.com/api/v1/courses/".$courseid[0]."/assignments?include[]=submission";
    $assignmentResource = getAssignment($access_token);
    
    $progress = 0;
    foreach($assignmentResource as $assignment) {
    
        $assnid = $assignment['id'];
        $assngrade = $assignment['submission']['grade'];
        $courseid2 = $assignment['course_id'];
        $assntotal++;
            if($assngrade != "" && $assngrade != "-" && $assngrade !=0 && $assngrade != 'null') {
                $progress++;
            }
    }
    $dec = $progress/$assntotal;
    $perc = round((float)$dec * 100);

    echo"
    <div class='w3-light-grey' style='border-radius: 25px;'>
            <div class= 'w3-container w3-center' style='width: ".$perc."%; border-radius: 25px; background-color: #013245; color: white;'>".$perc."%
            </div>
        </div>
        <div class='w3-center'>This progress bar shows the completed percentage of your main course tasks (e.g. discussions, assignments, assessments). 
        It is not your grade. It does not include your daily checkpoints.</div>
        <br />
        ";
    }

?>


</div>
