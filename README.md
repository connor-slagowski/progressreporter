# ProgressReporter

An app built for showing progress through a college course. The user allows external access to information through OAuth2 verification which then shows a percentage of the course items that have been completed